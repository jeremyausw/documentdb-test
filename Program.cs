﻿using System;
using System.Security;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;

namespace ConsoleApplication
{
    public class Program
    {
        private const string Endpoint = "<document-db-endpoint-url>";
        private const string PrimaryKey = "<document-db-primary-key>";
        private const string PartitionKey = "<partition-key";

        public static void Main(string[] args)
        {
            var secure = new SecureString();
            foreach (char c in PrimaryKey)
            {
                secure.AppendChar(c);
            }

            DocumentClient client = new DocumentClient(
                serviceEndpoint: new Uri(Endpoint),
                authKey: secure);

            var query = client.CreateDocumentQuery<Document>(
                    documentCollectionOrDatabaseUri: UriFactory.CreateDocumentCollectionUri("SL8", "SL8"),
                    sqlExpression: "SELECT * FROM c",
                    feedOptions: new FeedOptions() { PartitionKey = new PartitionKey(PartitionKey)}
            ).AsDocumentQuery();
                
            Task.Run(async () => {
                var result = await query.ExecuteNextAsync();    

                var k = 3;
            }).Wait();

            
            
        }
    }
}
